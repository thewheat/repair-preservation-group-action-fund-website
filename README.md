## Getting Started

 - Install yarn v1 https://classic.yarnpkg.com/en/docs/install
 - Add the netlify dev tools and hugo with `yarn global add netlify-cli
   hugo-bin`
 - Install needed dependencies for the project with `yarn` in this directory
 - Run the site with `ntl dev`

### Testing stripe payments

Before running the site, create a `.env` file in this directory with contents
like this

```
STRIPE_PUBLISHABLE_KEY=pk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
STRIPE_SECRET_KEY=sk_test_XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
```

Replace the publishable and secret key values with your own test keys from
stripe https://dashboard.stripe.com/test/apikeys You should be able to open a
test version of the donation page from your local version of the site. Use
credit card 4242 4242 4242 4242 to attempt a test payment.
