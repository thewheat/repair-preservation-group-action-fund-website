const stripe = require('stripe')(process.env.STRIPE_SECRET_KEY);

exports.handler = async (req) => {
  let { price, email } = JSON.parse(req.body);

  (isNaN(price)) ? price = parseInt(price) : console.log('price input validated', price);
  price = Math.round(price * 100); // because JS is dumb and the math turns into a floating point error

  const stripeSession = await stripe.checkout.sessions.create({
    payment_method_types: ['card'],
    mode: 'payment',
    customer_email: email,
    billing_address_collection: 'auto',
    success_url: 'https://www.fighttorepair.org/checkout/success',
    cancel_url: 'https://www.fighttorepair.org/checkout/oops',
    line_items: [
      {
        name: 'Donation to Repair Preservation Group',
        description: 'Thanks for donating to our 501c3!',
        amount: price,
        currency: 'USD',
        quantity: 1
      }
    ],
  });

  return {
    statusCode: 200,
    body: JSON.stringify({
      sessionId: stripeSession.id,
      publishableKey: process.env.STRIPE_PUBLISHABLE_KEY
    })
  };
};
